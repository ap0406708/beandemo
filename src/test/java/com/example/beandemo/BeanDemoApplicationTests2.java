package com.example.beandemo;

import com.example.beandemo.service.BeanA;
import com.example.beandemo.service.BeanB;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootTest
class BeanDemoApplicationTests2 {

    @Autowired
    MyBean myBean;

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        BeanA beanA = (BeanA) context.getBean("beanA");
        BeanB beanB = (BeanB) context.getBean("beanB");

        beanA.test();
        beanA.test2();

        beanB.test();
        beanB.test2();

    }



}
