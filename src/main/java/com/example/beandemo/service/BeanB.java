package com.example.beandemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

//add @Lazy to solute The dependencies of some of the beans in the application context form a cycle:
//https://blog.csdn.net/m0_50513629/article/details/121660518

@Service
public class BeanB {

    @Autowired
    @Lazy
    private BeanA beanA;

    public BeanB() {
        System.out.println("BeanB Initializing");
    }

    public void setBeanA(BeanA beanA) {
        this.beanA = beanA;
    }

    public void test(){
        System.out.println("BeanB test");
    }

    public void test2(){
        System.out.println("test from A");
        beanA.test();
    }

}
