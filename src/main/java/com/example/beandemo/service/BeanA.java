package com.example.beandemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class BeanA {
    @Autowired
    @Lazy
    private  BeanB beanB;
    public void setBeanB(BeanB beanB) {
        this.beanB = beanB;
    }

    public BeanA() {
        System.out.println("BeanA Initializing");
    }

    public void test(){
        System.out.println("BeanA test");
    }

    public void test2(){
        System.out.println("test from B");
        beanB.test();
    }

}
